#include "pitches.h"
#include "button.h"
#include "buzzer.h"

#define PIN_BUTTON_SELECT 3
#define PIN_BUTTON_OFF 5
#define PIN_BUTTON_SPEED 4
#define PIN_BUZZER 6

Button buttonSelect(PIN_BUTTON_SELECT);
Button buttonOff(PIN_BUTTON_OFF);
Button buttonSpeed(PIN_BUTTON_SPEED);
Buzzer buzzer(PIN_BUZZER);

int notes[] = {NOTE_A4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE};
double durations[] = {8, 1, 4, 1};
int melodyLength = 4;

int notes3[] = {NOTE_A4, NNOTE_CS6, NOTE_G4, NOTE_CS4};
double durations3[] = {8, 6, 4, 2};
int melodyLength3 = 4;

unsigned long speeds[] = {25, 50, 100, 200, 400, 800};
int currentSpeed = 2;
int speedsLength = 6;

// maybe somewhere in the future we will have one more button...

// and the second melody
int first = NOTE_C6;
int second = NOTE_DS6;
int third = NOTE_F5;

int notes2[] = {
  first, NOTE_SILENCE, first, NOTE_SILENCE, 
  first, NOTE_SILENCE, second, NOTE_SILENCE, third, NOTE_SILENCE,
  first, NOTE_SILENCE, second, NOTE_SILENCE, third, NOTE_SILENCE,
  first, NOTE_SILENCE
  };
double durations2[] = {
  4, 1, 4, 1, 
  4, 1, 4, 1, 4, 1,
  4, 1, 4, 1, 4, 1,
  4, 2
  };
int melodyLength2 = 18;

int currentMelody = 0;
int melodiesCount = 3;

void setup()
{
    buzzer.setMelody(notes, durations, melodyLength);
}

void loop()
{
    buzzer.playSound();

    if (buttonOff.wasPressed())
    {
        buzzer.turnSoundOff();
    }

    if (buttonSelect.wasPressed())
    {
      currentMelody=(currentMelody+1)%melodiesCount;
        if (currentMelody == 0)
        {
            buzzer.setMelody(notes2, durations2, melodyLength2);
            buzzer.turnSoundOn();
        }
        if (currentMelody == 1)
        {
          buzzer.setMelody(notes3, durations3, melodyLength3);
          buzzer.turnSoundOn(); 
        }
        if (currentMelody == 2)
        {
           buzzer.setMelody(notes, durations, melodyLength);
           buzzer.turnSoundOn();
        }
        
    }

    if (buttonSpeed.wasPressed())
    {
        currentSpeed = (currentSpeed + 1)%speedsLength;
        buzzer.setNoteDuration(speeds[currentSpeed]);
    }
}
