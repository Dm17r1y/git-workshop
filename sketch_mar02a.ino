
#include <Wire.h>
#define KEYPAD_ADDR 8


const byte ROWS = 4;
const byte COLS = 4;
char value[ROWS][COLS] = {
  {'0', 'D', '0', '0'},
  {'L', '0', 'R', '0'},
  {'0', 'U', '0', '0'},
  {'0', '0', '0', '0'}
};

byte PinOut[ROWS] = { 5, 4, 3, 2 };
byte PinIn[COLS] = { 6, 7, 8, 9 };

void setup() {
  pinMode(2, OUTPUT); 
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);

  pinMode(6, INPUT);
  digitalWrite(6, HIGH);
  pinMode(7, INPUT);
  digitalWrite(7, HIGH);
  pinMode(8, INPUT);
  digitalWrite(8, HIGH);
  pinMode(9, INPUT);
  digitalWrite(9, HIGH);

  Serial.begin(9600);

  

  
  Wire.begin(KEYPAD_ADDR);
  Wire.onRequest(requestEvent);
  
}

char getKey()
{
  char k = '0';

  for (int i = 1; i <= 4; i++)
  {
    digitalWrite(PinOut[i - 1], LOW);
    for (int j = 1; j <= 4; j++)
    {
      if (digitalRead(PinIn[j - 1]) == LOW) 
      {
        k = value[i - 1][j - 1];
        
        
        Serial.println(k);
        if (k != '0')
        
          return k;
        
      }
    }
    digitalWrite(PinOut[i - 1], HIGH); 
  }

  return k;
}

char oldKey = '0';
char newKey = '0';


void loop()
{
  newKey = getKey();
  if (oldKey != newKey) {
    oldKey = newKey;
    if (newKey != '0')
      Serial.println(newKey);
  }
}



void requestEvent() {
  Wire.write(oldKey);
}
