#include <Wire.h>
#define KEYPAD_ADDR 8
#define DISPLAY_ADDR 9

void setup() {
  Wire.begin();       
  Serial.begin(9600); 
}

char key = '0';

void loop() {
  Wire.requestFrom(KEYPAD_ADDR, 1);

  while (Wire.available()) { 
    key = Wire.read(); 
    Serial.print(key); 
  }


  Wire.beginTransmission(DISPLAY_ADDR);
  Wire.write(key);
  Wire.endTransmission();
}
