#include "LedControl.h"
#include <Wire.h>
#define DISPLAY_ADDR 9


LedControl lc = LedControl(26, 22, 24, 1); //(DIN, CLK, CS, amount MAX7219)

char key;
char direction; 

int snakeX[36], snakeY[36], foodX, foodY, speed = 100, snakeSize;

boolean inGame = false; 

void setup() {
  lc.shutdown(0, false);
  lc.setIntensity(0, 8);
  lc.clearDisplay(0);
  Serial.begin(9600);
  Wire.begin(DISPLAY_ADDR);
  Wire.onReceive(receiveEvent);
 
  newGame();
}

void loop() {
  if (inGame) { 
    lc.clearDisplay(0);


    // when using I2C delete this 
    // read derection from Serial 
    if (Serial.available() > 0)
      key = Serial.read();

    if (key == 'D' && direction != 'U')
      direction = 'D';
    if (key == 'U' && direction != 'D')
      direction = 'U';
    if (key == 'R' && direction != 'L')
      direction = 'R';
    if (key == 'L' && direction != 'R')
      direction = 'L';

    move(direction);

    checkIfHitFood(); 
    checkIfHitSelf();
    drawSnake();
    drawFood();

    delay(speed); 
  }
}

void move(char dir) {
  for (int i = snakeSize - 1; i > 0; i--) {
    snakeX[i] = snakeX[i - 1];
    snakeY[i] = snakeY[i - 1];
  }

  if (dir == 'U') {
    if (snakeY[0] == 0) 
      snakeY[0] = 7;
    else
      snakeY[0]--;
  }
  else if (dir == 'D') {
    if (snakeY[0] == 7)
      snakeY[0] = 0;
    else
      snakeY[0]++;
  }
  else if (dir == 'L') {
    if (snakeX[0] == 0) 
      snakeX[0] = 7;
    else
      snakeX[0]--;
  }
  else if (dir == 'R') {
    if (snakeX[0] == 7)
      snakeX[0] = 0;
    else
      snakeX[0]++;
  }
}

void drawSnake() {
  for (int i = 0; i < snakeSize; i++) {
    lc.setLed(0, snakeY[i], snakeX[i], true);
  }
}

void drawFood() {
  lc.setLed(0, foodY, foodX, true);
    delay(50);
  lc.setLed(0, foodY, foodX, false);
}

void newFood() {
  int newFoodX = random(0, 8);
  int newFoodY = random(0, 8);
  while (isSnake(newFoodX, newFoodY)) { 
    newFoodX = random(0, 8);
    newFoodY = random(0, 8);
  }
  foodX = newFoodX;
  foodY = newFoodY;
}

void checkIfHitFood() {
  if (snakeX[0] == foodX && snakeY[0] == foodY) {
    snakeSize++;
    newFood();
  }
}

void checkIfHitSelf() {
  for (int i = 1; i < snakeSize - 1; i++) 
    if (snakeX[0] == snakeX[i] && snakeY[0] == snakeY[i]) 
      gameOver();
}

boolean isSnake(int x, int y) {
  for (int i = 0; i < snakeSize - 1; i++){
    if ((x == snakeX[i]) && (y == snakeY[i]))
      return true;
  }
  return false;
}

void newGame() {
  for (int i = 0; i < 36; i++) { 
    snakeX[i] = -1;
    snakeY[i] = -1;
  }

  snakeX[0] = 4;
  snakeY[0] = 8;
  key = 'U';
  direction = 'U';
  snakeSize = 1; 
  newFood(); 
  inGame = true;
}

void gameOver() {
  inGame = false;
  for (int x = 0; x < 8; x++) {
    for (int y = 0; y < 8; y++) {
      lc.setLed(0, y, x, true);
      delay(20);
      lc.setLed(0, y, x, false);
    }
  }
  newGame();
}

char oldkey = '0';


void receiveEvent(int howMany) {
  key = Wire.read();
  if (key != '0' && key != oldkey) {
    oldkey = key;
    Serial.println(key);
  }
}
